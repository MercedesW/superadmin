from django.contrib import admin
from django.urls import path, include
from events.admin import event_admin_site



urlpatterns = [
    path('entity-admin/', admin.site.urls),
    path('event-admin/', event_admin_site.urls),
    path('', include('entities.urls')),
]













# donde ponemos la contraseña:
admin.site.site_header = "UMSRA Admin"
# título en donde están declarados los modelos:
admin.site.site_title = "UMSRA Admin Portal"
# En la pestaña:
admin.site.index_title = "Welcome to UMSRA Researcher Portal"

