from django.contrib import admin
from django.urls import path, include
from . import views


urlpatterns = [
    path('registro', views.registro, name='registro'),
    path('registroc', views.registro_completo, name='registro_completo'),
    path('passform', views.pass_change_form, name='pass_change_form'),
    path('passdone', views.pass_change_done, name='pass_change_done'),
    path('login', views.login, name='login'),
    
    path('base_dash', views.base_dashboard, name='base_dashboard'),
    path('app_dashboard', views.dashboard, name='app_dashboard'),
    path('app_newproject', views.new_project, name='app_newproject'),
    path('app_account_detail', views.app_account_detail, name='app_account_detail'),
    path('new_team', views.new_team, name='new_team'),
    path('signout', views.dashboard, name='signout'),
    path('team', views.team, name='team'),
]