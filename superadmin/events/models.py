from django.db import models
from entities.models import Hero, Villain

class Epic(models.Model):
    name = models.CharField(max_length=255)
    participating_heroes = models.ManyToManyField(Hero)
    participating_villains = models.ManyToManyField(Villain)

    class Meta:
        verbose_name = "épico"
        verbose_name_plural = "Épicos"


class Event(models.Model):
    epic = models.ForeignKey(Epic, on_delete=models.CASCADE)
    details = models.TextField()
    years_ago = models.PositiveIntegerField()
    
    class Meta:
        verbose_name = "evento"
        verbose_name_plural = "Eventos"


class EventHero(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    hero = models.ForeignKey(Hero, on_delete=models.CASCADE)
    is_primary = models.BooleanField()

    class Meta:
        verbose_name = "evento de héroe"
        verbose_name_plural = "Eventos de Héroe"


class EventVillain(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    hero = models.ForeignKey(Villain, on_delete=models.CASCADE)
    is_primary = models.BooleanField()

    class Meta:
        verbose_name = "evento de villano"
        verbose_name_plural = "Eventos de Villano"
