# SuperAdmin

Tutorial de configuración del Admin de Django
"Django admin cookbook"

1. Cómo cambiar el texto ‘Django administration’?

Por default Django admin muestra ‘Django administration’. Vamos a cambiarlo por ‘UMSRA Administration’

El texto aparece en estos lugares:

Login Page
The listview page
The HTML title tag

Cambiamos esos valores agregandoesto en urls.py:

    admin.site.site_header = "UMSRA Admin"
    admin.site.site_title = "UMSRA Admin Portal"
    admin.site.index_title = "Welcome to UMSRA Researcher Portal"

2. Cómo cambiar los nombres de apps y de los campos?

En Apps.py correspondiente(agrego el verbose_name):

    class EventsConfig(AppConfig):
        name = 'events'
        verbose_name = 'Eventos'

En el models.py correspondiente(agrego la clase Meta):

    class Event(models.Model):
        ...
    
        class Meta:
            verbose_name = "evento"
            verbose_name_plural = "Eventos"

3. Cómo crear dos admin sites independientes?

Se puede tener dos admin site con la misma Django App.
Vamos a mantener el default admin para entities y crear una subclase de AdminSite para
events.

Reemplazamos lo que está en events/admin.py por:

    from django.contrib.admin import AdminSite
    class EventAdminSite(AdminSite):
        site_header = "UMSRA Events Admin"
        site_title = "UMSRA Events Admin Portal"
        index_title = "Welcome to UMSRA Researcher Events Portal"

    event_admin_site = EventAdminSite(name='event_admin')


    event_admin_site.register(Epic)
    event_admin_site.register(Event)
    event_admin_site.register(EventHero)
    event_admin_site.register(EventVillain)

y en superadmin/urls.py reemplazamos tooodo, por:

    from events.admin import event_admin_site
    urlpatterns = [
        path('entity-admin/', admin.site.urls),
        path('event-admin/', event_admin_site.urls),
    ]

4. Cómo remover las default apps del Django admin?

Django incluye django.contrib.auth en INSTALLED_APPS.
Los modelos User y Groups son incluidos en el admin automáticamente.
Para removerlos, en entities/admin.py:

    from django.contrib.auth.models import User, Group
    admin.site.unregister(User)
    admin.site.unregister(Group)

5. Cómo agregar un logo al Django admin?

En settings.py:

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [os.path.join(BASE_DIR, 'templates/')],
            'APP_DIRS': True,
            ...
        },
    ]

Agregamos un directorio Templates/
Ahora copiamos el archivo base_site.html que está en:

    env/lib/python3.8/site-package/django/contrib/admin/templates/admin/base_site.html

y lo ponemos en Templates/ y lo modificamos(ver archivo).
Agregamos un directorio static/ y dentro agregamos la imagen del logo.

- Para reescribir el admin:
    https://docs.djangoproject.com/en/dev/ref/contrib/admin/#overriding-admin-templates
