from django.shortcuts import render



def base_dashboard(request):
    return render(request, 'entities/dashboard/base_dashboard.html')

def dashboard(request):
    return render(request, 'entities/dashboard/dashboard.html')

def new_project(request):
    return render(request, 'entities/dashboard/newproject.html')

def app_account_detail(request):
    return render(request, 'entities/dashboard/account_details.html')

def new_team(request):
    return render(request, 'entities/dashboard/new_team.html')

def team(request):
    return render(request, 'entities/dashboard/team.html')

def registro(request):
    return render(request, 'entities/registration_form.html')


def registro_completo(request):
    return render(request, 'entities/registration_complete.html')


def pass_change_form(request):
    return render(request, 'entities/password_change_form.html')


def pass_change_done(request):
    return render(request, 'entities/password_change_done.html')


def login(request):
    return render(request, 'entities/login.html')