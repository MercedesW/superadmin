from django.contrib import admin
from entities.models import Category, Origin, Entity, Hero, Villain
from django.contrib.auth.models import User, Group


admin.site.unregister(User)
admin.site.unregister(Group)

admin.site.register(Category)
admin.site.register(Origin)
admin.site.register(Hero)
admin.site.register(Villain)
